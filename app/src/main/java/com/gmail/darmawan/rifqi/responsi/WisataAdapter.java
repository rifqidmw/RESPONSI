package com.gmail.darmawan.rifqi.responsi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmail.darmawan.rifqi.responsi.model.Wisata;

import java.util.ArrayList;

/**
 * Created by yolo on 01/08/18.
 */

public class WisataAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Wisata> recordList;

    public WisataAdapter(Context context, int layout, ArrayList<Wisata> recordList) {
        this.context = context;
        this.layout = layout;
        this.recordList = recordList;
    }

    @Override
    public int getCount() {
        return recordList.size();
    }

    @Override
    public Object getItem(int i) {
        return recordList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView txtNama, txtAlamat, txtKeterangan;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if (row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.txtNama = row.findViewById(R.id.txtNama);
            holder.txtAlamat = row.findViewById(R.id.txtAlamat);
            holder.txtKeterangan = row.findViewById(R.id.txtKeterangan);
            holder.imageView = row.findViewById(R.id.imgIcon);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder)row.getTag();
        }

        Wisata wisata = recordList.get(i);

        holder.txtNama.setText(wisata.getNama());
        holder.txtAlamat.setText(wisata.getAlamat());
        holder.txtKeterangan.setText(wisata.getKeterangan());

        byte[] recordImage = wisata.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(recordImage, 0, recordImage.length);
        holder.imageView.setImageBitmap(bitmap);

        return row;
    }
}
