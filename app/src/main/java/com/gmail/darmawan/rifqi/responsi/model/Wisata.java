package com.gmail.darmawan.rifqi.responsi.model;

/**
 * Created by yolo on 01/08/18.
 */

public class Wisata {
    private int id;
    private String nama;
    private String alamat;
    private String keterangan;
    private byte[] image;

    public Wisata(int id, String nama, String alamat, String keterangan, byte[] image) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.keterangan = keterangan;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
